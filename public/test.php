<?php
$re = '/#(\d{4}) ?([a-zA-Z]{5})/';
$txt = '#0001 AAACD';
$sql = "SELECT q.id,q.full_mark_counter,q1.answer_key as a1,q2.answer_key as a2,q3.answer_key as a3,q4.answer_key as a4,q5.answer_key as a5 FROM `quiz` AS q LEFT JOIN `question_bank` as q1 on q1.id=q.`Q1_id` LEFT JOIN `question_bank` as q2 on q2.id=q.`Q2_id` LEFT JOIN `question_bank` as q3 on q3.id=q.`Q3_id` LEFT JOIN `question_bank` as q4 on q4.id=q.`Q4_id` LEFT JOIN `question_bank` as q5 on q5.id=q.`Q5_id` WHERE q.id=2";
$answer_arr = [
    'id' => 2,
    'full_mark_counter' => 1,
    'a1' => 'a',
    'a2' => 'a',
    'a3' => 'a',
    'a4' => 'c',
    'a5' => 'd',
];

if (preg_match($re, $txt, $matches, PREG_OFFSET_CAPTURE, 0)) {
//    echo "试卷代码：" . intval($matches[1][0]) . ",答案：" . $matches[2][0];
    $answers = str_split($matches[2][0]);
    $data = [
        'uid' => 11547457,
        'uname' => "胡三光",
        'quiz' => 2,
    ];
    $score = 0;
    $data['total_correct'] = 0;
    foreach ($answers as $key => $value) {
//        echo "第" . ($key + 1) . "题" . (strtolower($value) === strtolower($answer_arr['a' . ($key + 1)]) ? "正确" : "错误") . "<br>";
        $data['q' . ($key + 1) . '_answer'] = strtolower($value);
        if (strtolower($value) === strtolower($answer_arr['a' . ($key + 1)])) {
            $score += 20;
            $data['total_correct'] += 1;
            $data['q' . ($key + 1) . '_status'] = 1;
        } else {
            $data['q' . ($key + 1) . '_status'] = 0;
        }
    }

    if ($score === 100) {
        $score -= intval($answer_arr['full_mark_counter']) > 10 ? 10 : intval($answer_arr['full_mark_counter']);
    }

    $data['score'] = $score;
    echo json_encode($data);
} else {
    echo $txt . "不属于";
}