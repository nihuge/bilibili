<?php

namespace app\index\controller;

use app\index\model\bulletScreen;
use app\index\model\gifts;
use app\index\model\newFans;
use app\index\model\quiz;
use app\index\model\answer;
use think\Db;
use think\Exception;
use think\facade\Log;
use think\Request;
use think\Controller;


class Index extends Controller
{
    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V5.1<br/><span style="font-size:30px">12载初心不改（2006-2018） - 你值得信赖的PHP框架</span></p></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=64890268" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="eab4b9f840753f8e7"></think>';
    }

    public function sync(Request $request)
    {
        if ($request->isPost()) {

//            'steamed stuffed bun'
            $data = $request->put();
            if ($data) {
                if ($data['pwd'] === 'steamed stuffed bun') {
                    $bulletScreen = new bulletScreen();
                    Db::startTrans();
                    try {
                        $msg_arr = $data['list'][0];
                        $res = $bulletScreen->insert($msg_arr);
                    } catch (\Exception $e) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'sql Exception', 'e' => $e->getMessage()]);
                    }


                    try {
                        $re = '/#(\d{4}) ?([a-zA-Z]{5})/';

                        if (preg_match($re, $msg_arr['content'], $matches, PREG_OFFSET_CAPTURE, 0)) {
                            $quiz = new quiz();


                            $quiz_id = intval($matches[1][0]);
                            $answers = str_split($matches[2][0]);


                            $answer_arr = $quiz
                                ->alias('q')
                                ->field(['q.id', 'q.full_mark_counter', 'q1.answer_key' => 'a1', 'q2.answer_key' => 'a2', 'q3.answer_key' => 'a3', 'q4.answer_key' => 'a4', 'q5.answer_key' => 'a5'])
                                ->leftJoin(['question_bank' => 'q1'], 'q1.id=q.Q1_id')
                                ->leftJoin(['question_bank' => 'q2'], 'q2.id=q.Q2_id')
                                ->leftJoin(['question_bank' => 'q3'], 'q3.id=q.Q3_id')
                                ->leftJoin(['question_bank' => 'q4'], 'q4.id=q.Q4_id')
                                ->leftJoin(['question_bank' => 'q5'], 'q5.id=q.Q5_id')
                                ->where('q.id', '=', $quiz_id)
                                ->find();
                            if (isset($answer_arr['id'])) {
                                $answer = new answer();
                                if ($answer->where([['uid', '=', $msg_arr['uid']], ['quiz', '=', $quiz_id]])->count('*') == 0) {
                                    $data = [
                                        'uid' => $msg_arr['uid'],
                                        'uname' => $msg_arr['uname'],
                                        'quiz' => $quiz_id,
                                    ];

                                    $score = 0;
                                    $data['total_correct'] = 0;
                                    foreach ($answers as $key => $value) {
                                        $data['q' . ($key + 1) . '_answer'] = strtolower($value);
                                        if (strtolower($value) === strtolower($answer_arr['a' . ($key + 1)])) {
                                            $score += 20;
                                            $data['total_correct'] += 1;
                                            $data['q' . ($key + 1) . '_status'] = 1;
                                        } else {
                                            $data['q' . ($key + 1) . '_status'] = 0;
                                        }
                                    }

                                    if ($score === 100) {
                                        $score -= intval($answer_arr['full_mark_counter']) > 10 ? 10 : intval($answer_arr['full_mark_counter']);
                                        $quiz->where('id', '=', $quiz_id)
                                            ->inc('full_mark_counter')
                                            ->update();
                                    }

                                    $data['score'] = $score;

                                    $answer->insert($data);


                                }
                            }
                        }
                    } catch (\Exception $e) {
//                        Db::rollback();
//                        return json(['code' => 0, 'msg' => 'sql Exception', 'e' => $e->getMessage()]);
                    }

                    if ($res === false) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'save fail']);
                    } else {
                        Db::commit();
                        return json(['code' => 1, 'msg' => 'success']);
                    }


                } else {
                    return json(['code' => 0, 'msg' => 'password error']);
                }
            } else {
                return json(['code' => 0, 'msg' => 'method error']);
            }
        } else {
            return json(['code' => 0, 'msg' => 'method error']);
        }
    }

    public function sync_gift(Request $request)
    {
        if ($request->isPost()) {
//            'steamed stuffed bun'
            $data = $request->put();
            if ($data) {
                if ($data['pwd'] === 'steamed stuffed bun') {
                    $gift = new gifts();
                    Db::startTrans();
                    try {
//                        foreach ($data['list'] as $value) {
//                            Log::record($value);
                        $res = $gift->insertAll($data['list']);

//                        }
                    } catch (\Exception $e) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'sql Exception', 'e' => $e->getMessage()]);
                    }

                    if ($res === false) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'save fail']);
                    } else {
                        Db::commit();
                        return json(['code' => 1, 'msg' => 'success']);
                    }


                } else {
                    return json(['code' => 0, 'msg' => 'password error']);
                }
            } else {
                return json(['code' => 0, 'msg' => 'method error']);
            }
        } else {
            return json(['code' => 0, 'msg' => 'method error']);
        }
    }

    public function sync_subscribe(Request $request)
    {
        if ($request->isPost()) {
//            'steamed stuffed bun'
            $data = $request->put();
            if ($data) {
                if ($data['pwd'] === 'steamed stuffed bun') {
                    $newFans = new newFans();
                    Db::startTrans();
                    try {
//                        foreach ($data['list'] as $value) {
//                            Log::record($value);
                        $res = $newFans->insertAll($data['list']);

//                        }
                    } catch (\Exception $e) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'sql Exception', 'e' => $e->getMessage()]);
                    }

                    if ($res === false) {
                        Db::rollback();
                        return json(['code' => 0, 'msg' => 'save fail']);
                    } else {
                        Db::commit();
                        return json(['code' => 1, 'msg' => 'success']);
                    }


                } else {
                    return json(['code' => 0, 'msg' => 'password error']);
                }
            } else {
                return json(['code' => 0, 'msg' => 'method error']);
            }
        } else {
            return json(['code' => 0, 'msg' => 'method error']);
        }
    }
}
