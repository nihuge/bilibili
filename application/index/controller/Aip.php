<?php


namespace app\index\controller;

use Aip\AipSpeech;
use tencent\TTS;
use tencent\Config;
use think\Request;
use think\Controller;

class Aip extends Controller
{

    public function read(Request $request)
    {
        if ($request->isGet()) {
            if ($request->has('text', 'get')) {
                $text = $request->get("text");
                $per = $request->get("per", '');
                if ($per) {
                    $pa = array('per' => $per);
                } else {
                    $pa = array('per' => '111');
                }
                $Aip = new AipSpeech('23648486', '1yd2w5E7YPanElQ37A1lPSbE', 'lCFD2it0Fg7deyF8BLykmNLjbnfgejpr');
                $res = $Aip->synthesis($text, 'zh', 1, $pa);
//                $request->withHeader(array('Content-Type'=>"audio/mp3"));
                return response()->data($res)->header(array('Content-Type' => "audio/mp3"));
            } else {
                $this->error("访问错误");
            }
        } else {
            $this->error("访问错误");
        }
    }

    public function tread(Request $request)
    {
        if ($request->isGet()) {
            if ($request->has('text', 'get')) {
                $text = $request->get("text");
                $tts = new TTS();
                Config:: $TEXT = $text;
                Config:: $SESSION_ID = $tts->guid();
                Config:: $CODEC = 'mp3';
                Config:: $VOICET_YPE = 101016;
                //echo "Session id : " . Config :: $SESSION_ID . "\n";

                # 2. 调用获取mp3格式音频
                $result = $tts->getVoice();
//                dump($result);
//                $request->withHeader(array('Content-Type'=>"audio/mp3"));
                return response()->data($result)->header(array('Content-Type' => "audio/mp3"));
            } else {
                $this->error("访问错误");
            }
        } else {
            $this->error("访问错误");
        }
    }
}